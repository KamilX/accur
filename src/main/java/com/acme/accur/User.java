package com.acme.accur;

import lombok.Data;

@Data
public class User {
    private String pesel;
    private String name;
    private Integer accountSaldo;
}
