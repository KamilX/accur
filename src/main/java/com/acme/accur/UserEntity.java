package com.acme.accur;


import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
@AllArgsConstructor
public class UserEntity {

    @Id
    private String pesel;
    private String name;
    private Integer accountBalans;

    public UserEntity() {

    }

}
