package com.acme.accur;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccurApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccurApplication.class, args);
	}

}
