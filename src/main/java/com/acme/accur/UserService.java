package com.acme.accur;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public void saveUser(User user) {
        String pesel = user.getPesel();
        UserEntity userEntity = new UserEntity(user.getPesel(), user.getName(), user.getAccountSaldo());
        userRepository.save(userEntity);

        System.out.println("user saved");
        System.out.println("user record in base:");
        System.out.println(userRepository.findById(pesel));
    }
}
