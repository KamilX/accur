package com.acme.accur;

import com.acme.accur.model.CurrencyRate;
import com.acme.accur.model.MidRate;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping("/create")
    public String createUser(@RequestBody User user) {
        userService.saveUser(user);
        return "ok";
    }


    @GetMapping("/currency2")
    public void currency2() {
        ObjectMapper objectMapper = new ObjectMapper();
        CurrencyRate currencyRate = null;
        URL url = null;
        try {
            url = new URL("https://api.nbp.pl/api/exchangerates/rates/a/usd/");
            currencyRate = objectMapper.readValue(url, CurrencyRate.class);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(currencyRate.toString());
    }



    @GetMapping("/currency/usd")
    public String getUsdRate() {

        JSONObject json = null;
        try {
            json = readJsonFromUrl("https://api.nbp.pl/api/exchangerates/rates/a/usd/");
        } catch (IOException e) {
            e.printStackTrace();
        }


//        ObjectMapper mapper = new ObjectMapper();
//        JsonNode actualObj = null;
//        try {
//            actualObj = mapper.readTree(json.toString());
//        } catch (JsonProcessingException e) {
//            e.printStackTrace();
//        }
//
//        return actualObj.toPrettyString();

//        System.out.println(json);
//        return json.toString();
        return "ok";
    }


    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }


    public static JSONObject readJsonFromUrl(String url) throws IOException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);

            ObjectMapper objectMapper = new ObjectMapper();
//            MidRate currencyRate = objectMapper.readValue(jsonText, MidRate.class);
            Double midRate = ((Map<String, Double>) ((List) objectMapper.readValue(jsonText, MidRate.class).getDetails().get("rates")).get(0)).get("mid");
            System.out.println("MID RATE: " + midRate);


            Map<String, String> map = new HashMap<>();
            map.put("data", jsonText);
            JSONObject json = new JSONObject(map);
            json.get("data");
            return json;
        } finally {
            is.close();
        }
    }
}
