package com.acme.accur.model;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

public class CurrencyRate {
    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

//    public Map<String, String> getRates() {
//        return rates;
//    }
//
//    public void setRates(Map<String, String> rates) {
//        this.rates = rates;
//    }

    @JsonProperty
    String table;
    @JsonProperty
    String currency;
    @JsonProperty
    String code;
//    @JsonProperty
//    Map<String, String> rates;
}
